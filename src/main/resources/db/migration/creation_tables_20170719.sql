create table user_version (
  id BIGINT  PRIMARY KEY AUTO_INCREMENT,
  alias VARCHAR(255) NOT NULL DEFAULT '',
  created_at timestamp DEFAULT current_timestamp,
  updated_at timestamp DEFAULT current_timestamp
)